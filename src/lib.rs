#![allow(bad_style)]
#![allow(dead_code)]
#![allow(unused)]

extern crate http;
use http::header::HeaderValue;
use std::collections::HashMap;
pub use http::{Request, Response, StatusCode};
use std::time::{SystemTime, Duration};
use std::borrow::Cow;

// rfc7231 6.1
const status_code_cacheable_by_default: &[u16] = &[200, 203, 204, 206, 300, 301, 404, 405, 410, 414, 501];

// This implementation does not understand partial responses (206)
const understood_statuses: &[u16] = &[200, 203, 204, 300, 301, 302, 303, 307, 308, 404, 405, 410, 414, 501];

const hop_by_hop_headers: &[&str] = &[
    "date", // included, because we add Age update Date
    "connection", "keep-alive", "proxy-authenticate", "proxy-authorization", "te", "trailer", "transfer-encoding", "upgrade",
];
const excluded_From_Revalidation_Update: &[&str] = &[
    // Since the old body is reused, it doesn't make sense to change properties of the body
    "content-length", "content-encoding", "transfer-encoding",
    "content-range",
];

fn parse_cache_control<'a>(header: Option<&'a HeaderValue>) -> HashMap<&'a str, Option<&'a str>> {
    if let Some(header) = header.and_then(|h| h.to_str().ok()) {
        // TODO: When there is more than one value present for a given directive (e.g., two Expires header fields, multiple Cache-Control: max-age directives),
        // the directive's value is considered invalid. Caches are encouraged to consider responses that have invalid freshness information to be stale
        header.split(',')
        .map(|part| {
            let mut kv = part.splitn(2, '=');
            let k = kv.next().unwrap().trim();
            let v = kv.next()
                .map(|v| v.trim())
                .map(|v| {
                    let b = v.as_bytes();
                    if b.len() > 2 && b[0] == b'"' && b[b.len()-1] == b'"' {
                        &v[1..v.len()-1] // TODO: unescaping?
                    } else {
                        v
                    }
                });
            (k, v)
        }).collect()
    } else {
        HashMap::new()
    }
}

fn format_cache_control(cc: &HashMap<&str, Option<&str>>) -> Option<String> {
    let parts: Vec<Cow<str>> = cc.iter().map(|(&k, &v)| {
        if let Some(v) = v {
            format!("{}={}", k, v).into()
        } else {
            k.into()
        }
    }).collect();
    if parts.is_empty() {
        None
    } else {
        Some(parts.join(", "))
    }
}

pub struct CachePolicySettings {
    pub shared: bool,
    pub cache_heuristic: Option<f32>,
    pub immutable_min_ttl: Option<Duration>,
    pub ignore_cargo_cult: bool,
    pub trust_server_date: bool,
}

pub struct CachePolicy<'a, T: 'a, U: 'a> {
    req: &'a Request<T>,
    res: &'a Response<U>,
    is_shared: bool,
    trust_server_date: bool,
    cache_heuristic: f32,
    immutable_min_ttl: Duration,
    response_time: SystemTime,
    reqcc: HashMap<&'a str, Option<&'a str>>,
    rescc: HashMap<&'a str, Option<&'a str>>,
}

impl<'a, T, U> CachePolicy<'a, T, U> {
    pub fn new(req: &'a Request<T>, res: &'a Response<U>, CachePolicySettings {shared, cache_heuristic, immutable_min_ttl, ignore_cargo_cult, trust_server_date}: CachePolicySettings) -> Self {
        let mut c = Self {
            req,
            res,
            response_time: Self::now(),
            is_shared: shared,
            trust_server_date,
            cache_heuristic: cache_heuristic.unwrap_or(0.1), // 10% matches IE
            immutable_min_ttl: immutable_min_ttl.unwrap_or(Duration::from_secs(24*3600)),

            rescc: parse_cache_control(res.headers().get("cache-control")),
            reqcc: parse_cache_control(req.headers().get("cache-control")),
        };

        // Assume that if someone uses legacy, non-standard uncecessary options they don't understand caching,
        // so there's no point stricly adhering to the blindly copy&pasted directives.
        if (ignore_cargo_cult && c.rescc.get("pre-check").is_some() && c.rescc.get("post-check").is_some()) {
            c.rescc.remove("pre-check");
            c.rescc.remove("post-check");
            c.rescc.remove("no-cache");
            c.rescc.remove("no-store");
            c.rescc.remove("must-revalidate");
            // FIXME:
            // c.res_headers = Object.assign({}, c.res_headers, {"cache-control": format_cache_control(c.rescc)});
            // delete c.res_headers.expires;
            // delete c.res_headers.pragma;
        }

        // When the Cache-Control header field is not present in a request, caches MUST consider the no-cache request pragma-directive
        // as having the same effect as if "Cache-Control: no-cache" were present (see Section 5.2.1).
        if (res.headers().get("cache-control").is_none() && res.headers().get("pragma").and_then(|v| v.to_str().ok()).map_or(false, |v| v.contains("no-cache"))) {
            c.rescc.insert("no-cache", None);
        }
        c
    }

    fn now() -> SystemTime {
        SystemTime::now()
    }

    pub fn storable(&self) -> bool {
        // The "no-store" request directive indicates that a cache MUST NOT store any part of either this request or any response to it.
        return (self.reqcc.get("no-store").is_none() &&
            // A cache MUST NOT store a response to any request, unless:
            // The request method is understood by the cache and defined as being cacheable, and
            ("GET" === self.method || "HEAD" === self.method || ("POST" === self.method && self.has_explicit_expiration())) &&
            // the response status code is understood by the cache, and
            understood_statuses.index_Of(self.status) !== -1 &&
            // the "no-store" cache directive does not appear in request or response header fields, and
            !self.rescc["no-store"] &&
            // the "private" response directive does not appear in the response, if the cache is shared, and
            (!self.is_shared || !self.rescc.private) &&
            // the Authorization header field does not appear in the request, if the cache is shared,
            (!self.is_shared || self.no_authorization || self.allows_storing_authenticated()) &&
            // the response either:
            (
                // contains an Expires header field, or
                self.res_headers.expires ||
                // contains a max-age response directive, or
                // contains a s-maxage response directive and the cache is shared, or
                // contains a public response directive.
                self.rescc.public || self.rescc["max-age"] || self.rescc["s-maxage"] ||
                // has a status code that is defined as cacheable by default
                status_code_cacheable_by_default.index_Of(self.status) !== -1
            ));
    }
}
/*
    _has_explicit_expiration() {
        // 4.2.1 Calculating Freshness Lifetime
        return (self.is_shared && self.rescc["s-maxage"]) ||
            self.rescc["max-age"] ||
            self.res_headers.expires;
    }

    _assert_Request_hasHeaders(req) {
        if (!req || !req.headers) {
            throw Error("Request headers missing");
        }
    }

    satisfies_Without_Revalidation(req) {
        self.assert_Request_hasHeaders(req);

        // When presented with a request, a cache MUST NOT reuse a stored response, unless:
        // the presented request does not contain the no-cache pragma (Section 5.4), nor the no-cache cache directive,
        // unless the stored response is successfully validated (Section 4.3), and
        const requestCC = parse_cache_control(req.headers()["cache-control"]);
        if (requestCC["no-cache"] || /no-cache/.test(req.headers.pragma)) {
            return false;
        }

        if (requestCC["max-age"] && self.age() > requestCC["max-age"]) {
            return false;
        }

        if (requestCC["min-fresh"] && self.time_ToLive() < 1000*requestCC["min-fresh"]) {
            return false;
        }

        // the stored response is either:
        // fresh, or allowed to be served stale
        if (self.stale()) {
            const allows_stale = requestCC["max-stale"] && !self.rescc["must-revalidate"] && (true === requestCC["max-stale"] || requestCC["max-stale"] > self.age() - self.max_age());
            if (!allows_stale) {
                return false;
            }
        }

        return self.request_Matches(req, false);
    }

    _request_Matches(req, allow_head_Method) {
        // The presented effective request URI and that of the stored response match, and
        return (!self.url || self.url === req.url) &&
            (self.host === req.headers.host) &&
            // the request method associated with the stored response allows it to be used for the presented request, and
            (!req.method || self.method === req.method || (allow_head_Method && "HEAD" === req.method)) &&
            // selecting header fields nominated by the stored response (if any) match those presented, and
            self.vary_Matches(req);
    }

    _allows_storing_authenticated() {
        //  following Cache-Control response directives (Section 5.2.2) have such an effect: must-revalidate, public, and s-maxage.
        return self.rescc["must-revalidate"] || self.rescc.public || self.rescc["s-maxage"];
    }

    _vary_Matches(req) {
        if (!self.res_headers.vary) {
            return true;
        }

        // A Vary header field-value of "*" always fails to match
        if (self.res_headers.vary === "*") {
            return false;
        }

        const fields = self.res_headers.vary.trim().to_Lower_case().split(/\s*,\s* /);
        for(const name of fields) {
            if (req.headers()[name] !== self.req_headers[name]) return false;
        }
        return true;
    }

    _copy_Without_hopByHop_headers(in_headers) {
        const headers = {};
        for(const name in in_headers) {
            if (hop_by_hop_headers[name]) continue;
            headers[name] = in_headers[name];
        }
        // 9.1.  Connection
        if (in_headers.connection) {
            const tokens = in_headers.connection.trim().split(/\s*,\s* /);
            for(const name of tokens) {
                delete headers[name];
            }
        }
        if (headers.warning) {
            const warnings = headers.warning.split(/,/).filter(warning => {
                return !/^\s*1[0-9][0-9]/.test(warning);
            });
            if (!warnings.length) {
                delete headers.warning;
            } else {
                headers.warning = warnings.join(",").trim();
            }
        }
        return headers;
    }

    response_headers() {
        const headers = self.copy_Without_hopByHop_headers(self.res_headers);
        const age = self.age();

        // A cache SHOULD generate 113 warning if it heuristically chose a freshness
        // lifetime greater than 24 hours and the response's age is greater than 24 hours.
        if (age > 3600*24 && !self.has_explicit_expiration() && self.max_age() > 3600*24) {
            headers.warning = (headers.warning ? `${headers.warning}, ` : "") + "113 - "rfc7234 5.5.4"";
        }
        headers.age = `${Math.round(age)}`;
        headers.date = new Date(self.now()).toUTCString();
        return headers;
    }

    /**
     * Value of the Date response header or current time if Date was demed invalid
     * @return timestamp
     */
    date() {
        if (self.trust_server_date) {
            return self.server_date();
        }
        return self.response_Time;
    }

    _server_date() {
        const date_Value = Date.parse(self.res_headers.date)
        if (is_Finite(date_Value)) {
            const max_clock_drift = 8*3600*1000;
            const clock_drift = Math.abs(self.response_Time - date_Value);
            if (clock_drift < max_clock_drift) {
                return date_Value;
            }
        }
        return self.response_Time;
    }

    /**
     * Value of the Age header, in seconds, updated for the current time.
     * May be fractional.
     *
     * @return Number
     */
    age() {
        let age = Math.max(0, (self.response_Time - self.date())/1000);
        if (self.res_headers.age) {
            let age_Value = self.age_Value();
            if (age_Value > age) age = age_Value;
        }

        const resident_Time = (self.now() - self.response_Time)/1000;
        return age + resident_Time;
    }

    _age_Value() {
        const age_Value = parse_Int(self.res_headers.age);
        return is_Finite(age_Value) ? age_Value : 0;
    }

    /**
     * Value of applicable max-age (or heuristic equivalent) in seconds. This counts since response's `Date`.
     *
     * For an up-to-date value, see `time_ToLive()`.
     *
     * @return Number
     */
    max_age() {
        if (!self.storable() || self.rescc["no-cache"]) {
            return 0;
        }

        // Shared responses with cookies are cacheable according to the RFC, but IMHO it"d be unwise to do so by default
        // so this implementation requires explicit opt-in via public header
        if (self.is_shared && (self.res_headers["set-cookie"] && !self.rescc.public && !self.rescc.immutable)) {
            return 0;
        }

        if (self.res_headers.vary === "*") {
            return 0;
        }

        if (self.is_shared) {
            if (self.rescc["proxy-revalidate"]) {
                return 0;
            }
            // if a response includes the s-maxage directive, a shared cache recipient MUST ignore the Expires field.
            if (self.rescc["s-maxage"]) {
                return parse_Int(self.rescc["s-maxage"], 10);
            }
        }

        // If a response includes a Cache-Control field with the max-age directive, a recipient MUST ignore the Expires field.
        if (self.rescc["max-age"]) {
            return parse_Int(self.rescc["max-age"], 10);
        }

        const default_MinTtl = self.rescc.immutable ? self.immutable_MinTtl : 0;

        const date_Value = self.server_date();
        if (self.res_headers.expires) {
            const expires = Date.parse(self.res_headers.expires);
            // A cache recipient MUST interpret invalid date formats, especially the value "0", as representing a time in the past (i.e., "already expired").
            if (Number.is_NaN(expires) || expires < date_Value) {
                return 0;
            }
            return Math.max(default_MinTtl, (expires - date_Value)/1000);
        }

        if (self.res_headers["last-modified"]) {
            const last_Modified = Date.parse(self.res_headers["last-modified"]);
            if (is_Finite(last_Modified) && date_Value > last_Modified) {
                return Math.max(default_MinTtl, (date_Value - last_Modified)/1000 * self.cache_heuristic);
            }
        }

        return default_MinTtl;
    }

    time_ToLive() {
        return Math.max(0, self.max_age() - self.age())*1000;
    }

    stale() {
        return self.max_age() <= self.age();
    }

    static from_Object(obj) {
        return new this(undefined, undefined, {_from_Object:obj});
    }

    _from_Object(obj) {
        if (self.response_Time) throw Error("Reinitialized");
        if (!obj || obj.v !== 1) throw Error("Invalid serialization");

        Self {
            response_time = obj.t,
            is_shared = obj.sh,
            cache_heuristic = obj.ch,
            immutable_min_ttl = obj.imm !== undefined ? obj.imm : 24*3600*1000,
            status = obj.st,
            res_headers = obj.resh,
            rescc = obj.rescc,
            method = obj.m,
            url = obj.u,
            host = obj.h,
            no_authorization = obj.a,
            req_headers = obj.reqh,
            reqcc = obj.reqcc,
        }
    }

    to_Object() {
        return {
            v:1,
            t: self.response_Time,
            sh: self.is_shared,
            ch: self.cache_heuristic,
            imm: self.immutable_MinTtl,
            st: self.status,
            resh: self.res_headers,
            rescc: self.rescc,
            m: self.method,
            u: self.url,
            h: self.host,
            a: self.no_authorization,
            reqh: self.req_headers,
            reqcc: self.reqcc,
        };
    }

    /**
     * Headers for sending to the origin server to revalidate stale response.
     * Allows server to return 304 to allow reuse of the previous response.
     *
     * Hop by hop headers are always stripped.
     * Revalidation headers may be added or removed, depending on request.
     */
    revalidation_headers(incoming_Req) {
        self.assert_Request_hasHeaders(incoming_Req);
        const headers = self.copy_Without_hopByHop_headers(incoming_Req.headers);

        // This implementation does not understand range requests
        delete headers["if-range"];

        if (!self.request_Matches(incoming_Req, true) || !self.storable()) { // revalidation allowed via HEAD
            // not for the same resource, or wasn't allowed to be cached anyway
            delete headers["if-none-match"];
            delete headers["if-modified-since"];
            return headers;
        }

        /* MUST send that entity-tag in any cache validation request (using If-Match or If-None-Match) if an entity-tag has been provided by the origin server. */
        if (self.res_headers.etag) {
            headers["if-none-match"] = headers["if-none-match"] ? `${headers["if-none-match"]}, ${self.res_headers.etag}` : self.res_headers.etag;
        }

        // Clients MAY issue simple (non-subrange) GET requests with either weak validators or strong validators. Clients MUST NOT use weak validators in other forms of request.
        const forbids_Weak_Validators = headers["accept-ranges"] || headers["if-match"] || headers["if-unmodified-since"] || (self.method && self.method != "GET");

        /* SHOULD send the Last-Modified value in non-subrange cache validation requests (using If-Modified-Since) if only a Last-Modified value has been provided by the origin server.
        Note: This implementation does not understand partial responses (206) */
        if (forbids_Weak_Validators) {
            delete headers["if-modified-since"];

            if (headers["if-none-match"]) {
                const etags = headers["if-none-match"].split(/,/).filter(etag => {
                    return !/^\s*W\//.test(etag);
                });
                if (!etags.length) {
                    delete headers["if-none-match"];
                } else {
                    headers["if-none-match"] = etags.join(",").trim();
                }
            }
        } else if (self.res_headers["last-modified"] && !headers["if-modified-since"]) {
            headers["if-modified-since"] = self.res_headers["last-modified"];
        }

        return headers;
    }

    /**
     * Creates new CachePolicy with information combined from the previews response,
     * and the new revalidation response.
     *
     * Returns {policy, modified} where modified is a boolean indicating
     * whether the response body has been modified, and old cached body can't be used.
     *
     * @return {Object} {policy: CachePolicy, modified: Boolean}
     */
    revalidated_Policy(request, response) {
        self.assert_Request_hasHeaders(request);
        if (!response || !response.headers) {
            throw Error("Response headers missing");
        }

        // These aren't going to be supported exactly, since one CachePolicy object
        // doesn't know about all the other cached objects.
        let matches = false;
        if (response.status !== undefined && response.status != 304) {
            matches = false;
        } else if (response.headers.etag && !/^\s*W\//.test(response.headers.etag)) {
            // "All of the stored responses with the same strong validator are selected.
            // If none of the stored responses contain the same strong validator,
            // then the cache MUST NOT use the new response to update any stored responses."
            matches = self.res_headers.etag && self.res_headers.etag.replace(/^\s*W\//,"") === response.headers.etag;
        } else if (self.res_headers.etag && response.headers.etag) {
            // "If the new response contains a weak validator and that validator corresponds
            // to one of the cache's stored responses,
            // then the most recent of those matching stored responses is selected for update."
            matches = self.res_headers.etag.replace(/^\s*W\//,"") === response.headers.etag.replace(/^\s*W\//,"");
        } else if (self.res_headers["last-modified"]) {
            matches = self.res_headers["last-modified"] === response.headers()["last-modified"];
        } else {
            // If the new response does not include any form of validator (such as in the case where
            // a client generates an If-Modified-Since request from a source other than the Last-Modified
            // response header field), and there is only one stored response, and that stored response also
            // lacks a validator, then that stored response is selected for update.
            if (!self.res_headers.etag && !self.res_headers["last-modified"] &&
                !response.headers.etag && !response.headers()["last-modified"]) {
                matches = true;
            }
        }

        if (!matches) {
            return {
                policy: new self.constructor(request, response),
                modified: true,
            }
        }

        // use other header fields provided in the 304 (Not Modified) response to replace all instances
        // of the corresponding header fields in the stored response.
        const headers = {};
        for(const k in self.res_headers) {
            headers[k] = k in response.headers && !excluded_From_Revalidation_Update[k] ? response.headers()[k] : self.res_headers[k];
        }

        const new_Response = Object.assign({}, response, {
            status: self.status,
            method: self.method,
            headers,
        });
        return {
            policy: new self.constructor(request, new_Response),
            modified: false,
        };
    }
};

*/
